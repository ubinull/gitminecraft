### [ubinull](https://gitlab.com/ubinull) / [GitMinecraft](https://gitlab.com/ubinull/gitminecraft)  

Starter pack to self-host your Minecraft server with the help of Git!  
Using [GitHub](https://github.com/) as the Git host is probably better because you can have bigger repositories there.  

---

Usage:  

1. Create a Git repository
2. Clone it to your local hard drive
3. Copy this repository into yours
    - You may want to delete the `assets` folder and `README.md` file. Its not needed for your server.
4. Open up `launch.sh` using your favorite text editor!
5. Look for `WEBHOOK=""` in the script, then paste a Discord Webhook's URL in there
    - If you want to run the script with a custom server jar or custom RAM allocation, you may also want to change the $RAM and $JAR variables
6. Finally, make a commit using `git commit -a -m "Init"`, then push it with `git push`
7. Start up `launch.sh` from bash now: `./launch.sh` (if you are on Windows, use `WSL` or the Git Bash)
    - If it returns `Permission denied`, execute this command: `chmod +x launch.sh`
8. Done! The **GitMinecraft** script will handle the rest!

---

Server updater (`updater.jar`) is made by [ServerJars](https://serverjars.com)

By running this script, you agree to Mojang's End User License Agreement, since the `eula.txt` is included and already accepted in this repository for convinience and ease of use.
