#!/bin/bash

# Paste Webhook URL here
WEBHOOK=""

# Only edit if you know what you're doing!
RAM="3G"
JAR="updater.jar"


# NEVER EDIT UNDER THIS LINE!
IP=$(curl -s ipinfo.io/ip)
DATE=$(date +"%Y.%m.%d %H:%M")

clear

echo "Syncing repository..."
git fetch
clear

echo "Downloading changes..."
git pull
clear

echo "Starting server in 3..."
sleep 1
clear
echo "Starting server in 2..."
sleep 1
clear
echo "Starting server in 1..."
sleep 1
clear
echo "Starting..."

# Send message with webhook
./discord.sh \
  --webhook-url="$WEBHOOK" \
  --username "GitMinecraft" \
  --avatar "https://gitlab.com/ubinull/gitminecraft/-/raw/main/assets/git.png" \
  --title "Server started!" \
  --description "IP Address: $IP" \
  --color "0x57F287" \
  --thumbnail "https://gitlab.com/ubinull/gitminecraft/-/raw/main/assets/bolt.png" \
  --timestamp

clear

# Start server
java -jar -Xmx$RAM $JAR nogui

clear
# After server stop:
./discord.sh \
  --webhook-url="$WEBHOOK" \
  --username "GitMinecraft" \
  --avatar "https://gitlab.com/ubinull/gitminecraft/-/raw/main/assets/git.png" \
  --title "Server stopped!" \
  --description "IP Address: $IP" \
  --color "0xED4245" \
  --thumbnail "https://gitlab.com/ubinull/gitminecraft/-/raw/main/assets/poweroff.png" \
  --timestamp


echo "Getting changes..."
git commit -m "$DATE"
clear

echo "Uploading changes..."
git push
clear

echo "Server shut down."
sleep 3
clear
